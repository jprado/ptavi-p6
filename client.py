"""!/usr/bin/python3."""
# -*- coding: utf-8 -*-
# Jorge Prado Fernandez

import socket
import sys

"""
Programa cliente que abre un socket a un servidor
"""

# Cliente UDP simple.

methods = ['INVITE', 'BYE']
if sys.argv[1] not in methods:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")

# Variables a utilizar
phrase = sys.argv[2].split('@')[-1]  # Recorto para obtener los datos
print(phrase)
SERVER = phrase.split(':')[0]
RECEPTOR = sys.argv[2].split('@')[0]
PORT = int(phrase.split(':')[-1])
METODO = sys.argv[1]
# Contenido que vamos a enviar
LINE = (METODO + " sip:" + RECEPTOR + "@" + SERVER + " SIP/2.0" + "\r\n\r\n")
LINE2 = ("ACK" + " sip:" + RECEPTOR + "@" + SERVER + " SIP/2.0" + "\r\n\r\n")

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORT))

    print("Enviando -> " + LINE)
    my_socket.send(bytes(LINE, 'utf-8'))
    data = my_socket.recv(1024)
    mensaje = data.decode('utf-8')  # Para comprobar lo que envía el servidor
    print(mensaje)
    if METODO == 'INVITE':
        if mensaje == 'SIP/2.0 100 Trying\r\n\r\n, SIP/2.0 180 Ringing\r\n\r\n, SIP/2.0 200 OK\r\n\r\n':
            print("Enviando -> " + LINE2)
            my_socket.send(bytes(LINE2, 'utf-8'))
    print("Terminando socket...")

print("Fin.")
