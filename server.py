"""!/usr/bin/python3."""
# -*- coding: utf-8 -*-
# Jorge Prado Fernandez

import os
import socketserver
import sys

"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

if len(sys.argv) != 4:
    sys.exit("Usage: python3 server.py IP port audio_file")
if sys.argv[3] != 'cancion.mp3':
    sys.exit("Usage: python3 server.py IP port audio_file")


class EchoHandler(socketserver.DatagramRequestHandler):
    """Echo server class."""

    def handle(self):
        """Manejador."""
        # Leyendo línea a línea lo que nos envía el cliente
        line = self.rfile.read()
        print("El cliente nos manda " + line.decode('utf-8'))
        # Metodo para identificar peticiones
        text = line.decode('utf-8')
        methods = ['INVITE', 'BYE', 'ACK']
        petition = text.split()
        SIP = petition[1].split(':')

        if petition[0] == 'INVITE':
            self.wfile.write(b"SIP/2.0 100 Trying\r\n\r\n, SIP/2.0 180 Ringing\r\n\r\n, SIP/2.0 200 OK\r\n\r\n")
            if SIP[0] != 'sip':
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")
            elif petition[2] != 'SIP/2.0':
                self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")

        elif petition[0] == 'ACK':
            fichero_audio = sys.argv[3]
            aEjecutar = 'mp32rtp -i 127.0.0.1 -p 23032 < ' + fichero_audio
            print("Vamos a ejecutar", aEjecutar)
            os.system(aEjecutar)

        elif petition[0] == 'BYE':
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")

        else:
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")


if __name__ == "__main__":
    # Creamos servidor de eco y escuchamos
    PORT = int(sys.argv[2])
    serv = socketserver.UDPServer(('', PORT), EchoHandler)
    print("Listening...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizando servidor")
